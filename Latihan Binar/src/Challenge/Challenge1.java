package Challenge;

import java.util.Scanner;

public class Challenge1 {

	public static void main(String[] args) {
	//TODO Auto-regenerated method stub
	boolean ulang = true;
	while (true) {
	String pilih_bidang;
	
	System.out.println("--------------------------------------");
	System.out.println("Kalkulator Penghitung Luas dan Volum");
	System.out.println("--------------------------------------");
	System.out.println("Menu");
	System.out.println("1. Hitung Luas Bidang");
	System.out.println("2. Hitung Volum");
	System.out.println("0. Tutup Aplikasi");
	System.out.println("--------------------------------------");
	
	Scanner input = new Scanner(System.in);
	String pilih = input.nextLine();
	switch(pilih) {
	case "1" : 
	System.out.println("--------------------------------------");
	System.out.println("Pilih bidang yang akan dihitung");
    System.out.println("--------------------------------------");
    System.out.println("1. Persegi");
    System.out.println("2. Lingkaran");
    System.out.println("3. Segitiga");
    System.out.println("4. Persegi Panjang");
    System.out.println("0. Kembali ke menu sebelumnya");
    System.out.println("--------------------------------------");
    	pilih_bidang = input.nextLine();
    		switch(pilih_bidang) {
    		case "1" : 
    		System.out.println("--------------------------------------");
    		System.out.println("Anda memilih persegi");
    		System.out.println("--------------------------------------");
    		System.out.print("Masukkan sisi : " );
   			int a = input.nextInt();
   			System.out.println("");
   			System.out.println("processing...");
    		System.out.println("");
    		System.out.println("Luas dari persegi adalah = " + a*a) ;
    		System.out.println("--------------------------------------");
   			System.out.println("tekan apa saja untuk kembali ke menu utama");	
   			continue;
    		case "2" :
    		System.out.println("--------------------------------------");
    		System.out.println("Anda memilih lingkaran");
    		System.out.println("--------------------------------------");
   			System.out.print("Masukkan jari-jari : " );
   			int b = input.nextInt();
   			System.out.println("");
    		System.out.println("processing...");
    		System.out.println("");
    		System.out.println("Luas dari lingkaran adalah = " + 3.14*b*b) ;
  			System.out.println("--------------------------------------");
    		System.out.println("tekan apa saja untuk kembali ke menu utama");
    		continue;
    		case "3" :
    		System.out.println("--------------------------------------");
    		System.out.println("Anda memilih segitiga");
    		System.out.println("--------------------------------------");
   			System.out.print("Masukkan alas   : " );
   			int c = input.nextInt();
   			System.out.print("Masukkan tinggi : ");
   			int d = input.nextInt();
    		System.out.println("");
    		System.out.println("processing...");
   			System.out.println("");
   			System.out.println("Luas dari segitiga adalah = " + (c*d)/2) ;
  			System.out.println("--------------------------------------");
    		System.out.println("tekan apa saja untuk kembali ke menu utama");
    		continue;
    		case "4" :
    		System.out.println("--------------------------------------");
    		System.out.println("Anda memilih persegi panjang");
    		System.out.println("--------------------------------------");
   			System.out.print("Masukkan panjang   : " );
   			int e = input.nextInt();
    		System.out.print("Masukkan lebar     : ");
    		int f = input.nextInt();
    		System.out.println("");
   			System.out.println("processing...");
   			System.out.println("");
   			System.out.println("Luas dari persegi panjang adalah = " + e*f) ;
    		System.out.println("--------------------------------------");
    		System.out.println("tekan apa saja untuk kembali ke menu utama");
    		continue;
			case "0" :
			continue;
    		}
    
	case "2" : 
	System.out.println("--------------------------------------");
	System.out.println("Pilih bidang yang akan dihitung");
    System.out.println("--------------------------------------");
    System.out.println("1. Kubus");
    System.out.println("2. Balok");
    System.out.println("3. Tabung");
    System.out.println("0. Kembali ke menu sebelumnya");
	System.out.println("--------------------------------------");	
		pilih_bidang = input.nextLine();
			switch(pilih_bidang) {
			case "1" : 
    		System.out.println("--------------------------------------");
    		System.out.println("Anda memilih kubus");
    		System.out.println("--------------------------------------");
    		System.out.print("Masukkan sisi : " );
   			int g = input.nextInt();
   			System.out.println("");
   			System.out.println("processing...");
   			System.out.println("");
    		System.out.println("Volum dari kubus adalah = " + g*g*g) ;
    		System.out.println("--------------------------------------");
   			System.out.println("tekan apa saja untuk kembali ke menu utama");
   			continue;
			case "2" :
		   	System.out.println("--------------------------------------");
	    	System.out.println("Anda memilih balok");
	   		System.out.println("--------------------------------------");
	   		System.out.print("Masukkan panjang : " );
	  		int h = input.nextInt();
	   		System.out.print("Masukkan lebar   : " );
	  		int i = input.nextInt();
	   		System.out.print("Masukkan tinggi  : " );
	  		int j = input.nextInt();
	  		System.out.println("");
	  		System.out.println("processing...");
	  		System.out.println("");
	   		System.out.println("Volum dari balok adalah = " + h*i*j) ;
	   		System.out.println("--------------------------------------");
   			System.out.println("tekan apa saja untuk kembali ke menu utama");
   			continue;
			case "3" :
			System.out.println("--------------------------------------");
	    	System.out.println("Anda memilih tabung");
	   		System.out.println("--------------------------------------");
		   	System.out.print("Masukkan tinggi    : " );
		  	int k = input.nextInt();
	  		System.out.print("Masukkan jari-jari : " );
	 		int l = input.nextInt();
		  	System.out.println("");
			System.out.println("processing...");
	 		System.out.println("");
	   		System.out.println("Volum dari tabung adalah = " + 3.14*k*(l*l)) ;
	   		System.out.println("--------------------------------------");
	   		System.out.println("tekan apa saja untuk kembali ke menu utama");
	   		continue;
			case "0" :
			continue;
			}	
			
	case "0" :
	System.exit(0);
		}
	}
	}
}
