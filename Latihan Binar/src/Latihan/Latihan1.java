package Latihan;

import java.util.Scanner;

public class Latihan1 {
	public static int solve(int jumlah) {
		if (jumlah == 1)
			return 1;
		else 
			return jumlah * solve(jumlah - 1);
	}

	public static void main(String[] args) {
	// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int angka = 1;
		while (true) {
			angka = input.nextInt();
			if (angka <= 0)
				break;
			System.out.println(solve(angka));
		}
		input.close();
	}

}
