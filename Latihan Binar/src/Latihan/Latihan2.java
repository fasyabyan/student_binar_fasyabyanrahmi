package Latihan;

import java.util.Scanner;

public class Latihan2 {
	public static int solve(int hewan) {
		if (hewan == 1)
			return 1;
		else 
			return hewan * solve(hewan - 1);
	}

	public static void main(String[] args) {
	// TODO Auto-generated method stub
		Scanner input = new Scanner(System.in);
		int angka = 1;
		while (true) {
			angka = input.nextInt();
			if (angka <= 0)
				break;
			System.out.println(solve(angka));
		}
		input.close();
	}

}
